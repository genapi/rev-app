INSERT INTO publishers (
  id,
  name,
  login,
  email
) VALUES (1, 'Some Name', 'bablo', 'bablo@bablo.ba');

INSERT INTO advertiser (
  id,
  name,
  collector
) VALUES (1, 'MVideo Moscow', 'MVIDEO_CLIENT');

INSERT INTO offer (
  id,
  name,
  key,
  advertiser_id
) VALUES (1, 'XBox 360', 'xbox-360', 1), (2, 'Plastasion 4', 'ps-4', 1);

INSERT INTO account (
  id,
  name,
  login,
  password,
  advertiser_id,
  is_active
) VALUES (1, 'MVideo gaming', 'mvideo_connect_pass', 'random', 1, true);

INSERT INTO publisher_account (
  id,
  name,
  pub_id,
  acc_id
) VALUES (1, 'bablo_xbox', 1, 1);