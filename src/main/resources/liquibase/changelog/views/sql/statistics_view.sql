CREATE OR REPLACE VIEW statistics_view AS
  SELECT sta.*, off.name AS offer_name, adv.name AS advertiser_name, adv.collector  FROM statistic AS sta
  LEFT JOIN offer AS off ON off.key = sta.offer_key AND off.advertiser_id = sta.advertiser_id
  LEFT JOIN advertiser AS adv ON adv.id = sta.advertiser_id