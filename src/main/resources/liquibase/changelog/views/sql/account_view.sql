CREATE OR REPLACE VIEW account_view AS
  SELECT acc.*,
          adv.name AS advertiser_name,
          adv.collector
  FROM account AS acc
  LEFT JOIN advertiser AS adv ON adv.id = acc.advertiser_id;