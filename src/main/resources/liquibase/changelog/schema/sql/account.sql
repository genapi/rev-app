CREATE TABLE account (
  id                BIGSERIAL,
  name              VARCHAR NOT NULL,
  login             VARCHAR NOT NULL,
  password          VARCHAR NOT NULL,
  advertiser_id     BIGINT NOT NULL,
  is_active         BOOlEAN,

  PRIMARY KEY (id),
  UNIQUE (name),
  FOREIGN KEY (advertiser_id) REFERENCES advertiser (id)
)