CREATE TABLE offer (
  id              BIGSERIAL,
  name            VARCHAR NOT NULL,
  key             VARCHAR NOT NULL,
  advertiser_id   BIGINT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name, advertiser_id),
  UNIQUE (key, advertiser_id),
  UNIQUE (name, key),
  FOREIGN KEY (advertiser_id) REFERENCES advertiser (id)
)