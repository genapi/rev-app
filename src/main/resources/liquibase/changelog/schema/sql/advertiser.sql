CREATE TABLE advertiser (
  id          BIGSERIAL,
  name        VARCHAR NOT NULL,
  collector   VARCHAR NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name)
)