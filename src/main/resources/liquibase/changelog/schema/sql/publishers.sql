CREATE TABLE publishers (
    id        BIGSERIAL,
    name      VARCHAR NOT NULL,
    login     VARCHAR NOT NULL,
    email     VARCHAR NOT NULL,

    PRIMARY KEY (id),
    UNIQUE (name),
    UNIQUE (login)
)