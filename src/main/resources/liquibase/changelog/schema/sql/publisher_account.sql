CREATE TABLE publisher_account (
  id        BIGSERIAL,
  name      VARCHAR,
  pub_id    BIGINT NOT NULL,
  acc_id    BIGINT NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (pub_id) REFERENCES publishers (id),
  FOREIGN KEY (acc_id) REFERENCES account (id)
)