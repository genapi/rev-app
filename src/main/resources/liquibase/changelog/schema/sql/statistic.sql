CREATE TABLE statistic (
  id              BIGSERIAL,
  publisher       VARCHAR,
  advertiser_id   BIGINT,
  offer_key       VARCHAR,
  amount          DECIMAL,
  comission       DECIMAL,
  earnings        DECIMAL,

  PRIMARY KEY (id),
  FOREIGN KEY (publisher) REFERENCES publishers (login),
  FOREIGN KEY (advertiser_id) REFERENCES advertiser (id)
)