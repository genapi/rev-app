package com.revtask.app.repository;

import com.revtask.app.jooq.tables.pojos.Statistic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatisticRepository extends JpaRepository<Statistic, Long> {
}
