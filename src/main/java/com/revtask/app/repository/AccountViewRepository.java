package com.revtask.app.repository;

import com.revtask.app.entity.AccountViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountViewRepository extends JpaRepository<AccountViewEntity, Long> {
    List<AccountViewEntity> getAllByIsActive(Boolean isActive);
}
