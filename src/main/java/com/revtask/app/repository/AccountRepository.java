package com.revtask.app.repository;

import com.revtask.app.jooq.tables.pojos.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    List<Account> getAllByIsActive(Boolean isActive);
}
