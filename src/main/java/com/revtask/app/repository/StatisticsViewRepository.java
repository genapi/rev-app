package com.revtask.app.repository;

import com.revtask.app.entity.StatisticsViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatisticsViewRepository extends JpaRepository<StatisticsViewEntity, Long> {
}
