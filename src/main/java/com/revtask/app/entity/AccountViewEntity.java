package com.revtask.app.entity;

import com.revtask.app.collector.CollectorClientEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "account_view")
public class AccountViewEntity {
    @Id
    private Long id;
    private String name;
    private String advertiserName;
    private String login;
    private String password;
    private Long advertiserId;
    private Boolean isActive;
    private String collector;

    public void setCollector(CollectorClientEnum collector) {
        this.collector = collector.toString();
    }

    public CollectorClientEnum getCollector() {
        return CollectorClientEnum.valueOf(this.collector);
    }
}
