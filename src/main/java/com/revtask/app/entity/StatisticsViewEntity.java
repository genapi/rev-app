package com.revtask.app.entity;

import com.revtask.app.collector.CollectorClientEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "statistics_view")
public class StatisticsViewEntity {
    @Id
    private Long id;
    private String publisher;
    private Long advertiserId;
    private String  offerKey;
    private Double amount;
    private Double comission;
    private Double earnings;
    private String offerName;
    private String advertiserName;
    private String collector;

    public void setCollector(CollectorClientEnum collector) {
        this.collector = collector.toString();
    }

    public CollectorClientEnum getCollector() {
        return CollectorClientEnum.valueOf(this.collector);
    }
}
