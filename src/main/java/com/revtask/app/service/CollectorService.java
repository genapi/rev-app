package com.revtask.app.service;

import com.revtask.app.collector.CollectorClientBootstrap;
import com.revtask.app.entity.StatisticsViewEntity;
import com.revtask.app.jooq.tables.pojos.Statistic;
import com.revtask.app.repository.AccountViewRepository;
import com.revtask.app.repository.StatisticRepository;
import com.revtask.app.repository.StatisticsViewRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class CollectorService {

    private final AccountViewRepository accountViewRepository;
    private final StatisticRepository statisticRepository;
    private final StatisticsViewRepository statisticsViewRepository;
    private final CollectorClientBootstrap collectorClientBootstrap;

    public void toCollectStatistics() {
        this.accountViewRepository.getAllByIsActive(true).forEach(acc -> {
            collectorClientBootstrap.collectStatistics(acc);
        });
    }

    public List<StatisticsViewEntity> getRows() {
        return statisticsViewRepository.findAll();
    }

}
