package com.revtask.app.dto;

import lombok.Data;

@Data
public class CollectorDTO {
    private String publisher;
    private Long advertiserId;
    private String offerKey;
    private Double saleAmount;
    private Double comission;
    private Double pubEarnings;
}
