package com.revtask.app.collector.clients;

import com.revtask.app.jooq.tables.pojos.Statistic;

import java.util.List;

public interface BaseCollectorClient {
    List<Statistic> getStatistics(String login, String password);
}
