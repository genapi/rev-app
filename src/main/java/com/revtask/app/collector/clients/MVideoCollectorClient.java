package com.revtask.app.collector.clients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.revtask.app.jooq.tables.pojos.Statistic;
import lombok.AllArgsConstructor;
import org.jooq.tools.json.JSONArray;
import org.jooq.tools.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Component
public class MVideoCollectorClient implements BaseCollectorClient {


    public List<Statistic> getStatistics(String login, String password) {
        List<JSONObject> getedData = new ArrayList<>();

        try {
            getedData = getData(login, password);
        } catch (IOException e){
            return new ArrayList<>();
        }

        return parseData(getedData);
    }

    public List<JSONObject> getData(String login, String password) throws IOException {
        String getedData = "[\n" +
                "\t{\n" +
                "\t\t\"attr_pub\": \"bablo\",\n" +
                "\t\t\"product\":\t\"xbox-360\",\n" +
                "\t\t\"amount\": \"10900\",\n" +
                "\t\t\"percent\": \"10\",\n" +
                "\t\t\"earning\": \"109\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"attr_pub\": \"bablo\",\n" +
                "\t\t\"product\":\t\"ps-4\",\n" +
                "\t\t\"amount\": \"25000\",\n" +
                "\t\t\"percent\": \"10\",\n" +
                "\t\t\"earning\": \"250\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"attr_pub\": \"bablo\",\n" +
                "\t\t\"product\":\t\"xbox-360\",\n" +
                "\t\t\"amount\": \"10900\",\n" +
                "\t\t\"percent\": \"10\",\n" +
                "\t\t\"earning\": \"109\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"attr_pub\": \"bablo\",\n" +
                "\t\t\"product\":\t\"ps-4\",\n" +
                "\t\t\"amount\": \"25000\",\n" +
                "\t\t\"percent\": \"10\",\n" +
                "\t\t\"earning\": \"250\"\n" +
                "\t}\n" +
                "]";

        ObjectMapper objectMapper = new ObjectMapper();
        JSONObject[] arr = objectMapper.readValue(getedData, JSONObject[].class);

        return Arrays.asList(arr);
    }

    public List<Statistic> parseData(List<JSONObject> arr) {
        List<Statistic> statistics = new ArrayList<>();

        arr.forEach(item -> {
            Statistic statItem = new Statistic();

            statItem.setPublisher((String) item.get("attr_pub"));
            statItem.setOfferKey((String) item.get("product"));
            statItem.setAmount(BigDecimal.valueOf(Double.valueOf((String) item.get("amount"))));
            statItem.setComission(BigDecimal.valueOf(Double.valueOf((String) item.get("percent"))));
            statItem.setEarnings(BigDecimal.valueOf(Double.valueOf((String) item.get("earning"))));

            statistics.add(statItem);
        });

        return statistics;
    }
}
