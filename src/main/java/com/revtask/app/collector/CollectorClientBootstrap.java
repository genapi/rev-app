package com.revtask.app.collector;

import com.revtask.app.collector.clients.BaseCollectorClient;
import com.revtask.app.collector.clients.MVideoCollectorClient;
import com.revtask.app.entity.AccountViewEntity;
import com.revtask.app.jooq.tables.pojos.Statistic;
import com.revtask.app.repository.StatisticRepository;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Component
public class CollectorClientBootstrap {

    private StatisticRepository statisticRepository;

    private MVideoCollectorClient mVideoCollectorClient;

    private Map<CollectorClientEnum, BaseCollectorClient> clients;

    @PostConstruct
    private void bootstrap() {
        clients.put(CollectorClientEnum.MVIDEO_CLIENT, mVideoCollectorClient);
    }

    @Async
    public void collectStatistics(AccountViewEntity accountViewEntity) {
        List<Statistic> statistic = this.clients.get(accountViewEntity.getCollector()).getStatistics(accountViewEntity.getLogin(), accountViewEntity.getPassword());
        statistic.forEach(stata -> {
            stata.setAdvertiserId(accountViewEntity.getAdvertiserId());
        });
        statisticRepository.saveAll(statistic);
        System.out.println(accountViewEntity.getCollector().toString() + ": is done");
    }

}
