package com.revtask.app.controller;

import com.revtask.app.entity.StatisticsViewEntity;
import com.revtask.app.jooq.tables.pojos.Statistic;
import com.revtask.app.service.CollectorService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/statistics")
public class StatisticsController {

    private final CollectorService collectorService;

    @GetMapping("/collect")
    public String toCollectStatistics() {
        collectorService.toCollectStatistics();
        return "ok";
    }

    @GetMapping("/get-rows")
    public List<StatisticsViewEntity> getRows() {
        return collectorService.getRows();
    }

}
